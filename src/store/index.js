import Vue from 'vue';
import Vuex from 'vuex';

import results from '../../results.json';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    flights: [],
    airlines: null,
    options: {
      direct: false,
      baggage: false,
      return: false,
    },
    selectedAirlines: [],
  },
  mutations: {
    setAirlines(state, data) {
      state.airlines = data;
    },
    setFlights(state, data) {
      state.flights = data;
    },
    addAirline(state, data) {
      state.selectedAirlines.push(data);
    },
    addAllAirlines(state, data) {
      state.selectedAirlines = data;
    },
    removeAirline(state, data) {
      if (state.selectedAirlines.includes(data)) {
        state.selectedAirlines = state.selectedAirlines.filter(
          (airline) => airline != data
        );
      }
    },
    removeAllAirlines(state) {
      state.selectedAirlines = [];
    },
    setOptions(state, data) {
      state.options[data.type] = data.status;
    },
    resetAll(state) {
      state.options.baggage = false;
      state.options.direct = false;
      state.options.return = false;
      state.selectedAirlines = Object.keys(state.airlines);
    },
  },
  actions: {
    async getAirlines({ commit }) {
      const response = await results.airlines;
      commit('setAirlines', response);
      commit('addAllAirlines', Object.keys(response));
    },
    async getFlights({ commit, state }) {
      const { options, selectedAirlines } = state;
      let response = await results.flights;
      response = response.filter((item) =>
        selectedAirlines.includes(item.validating_carrier)
      );
      if (options.direct)
        response = response.filter(
          (item) => item.itineraries[0][0].stops === 0
        );
      if (options.return) response = response.filter((item) => item.refundable);
      if (options.baggage)
        response = response.filter((item) => item.services['20KG']);
      commit('setFlights', response);
    },
    async addAirlineAction({ commit, dispatch }, payload) {
      commit('addAirline', payload);
      dispatch('getFlights');
    },
    async removeAirlineAction({ commit, dispatch }, payload) {
      commit('removeAirline', payload);
      dispatch('getFlights');
    },
    async addAllAirlinesAction({ commit, dispatch }, payload) {
      commit('addAllAirlines', payload);
      dispatch('getFlights');
    },
    async removeAllAirlinesAction({ commit, dispatch }) {
      commit('removeAllAirlines');
      dispatch('getFlights');
    },
    async setOptionsAction({ commit, dispatch }, payload) {
      commit('setOptions', payload);
      dispatch('getFlights');
    },
    async resetAllAction({ commit, dispatch }) {
      commit('resetAll');
      dispatch('getFlights');
    },
  },
  modules: {},
  getters: {
    flights: (state) => state.flights,
    airlines: (state) => state.airlines,
    selectedAirlines: (state) => state.selectedAirlines,
    options: (state) => state.options,
  },
});
