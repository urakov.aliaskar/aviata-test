import Vue from 'vue';
import VueI18n from 'vue-i18n';
import { Kz, Ru } from './locale';
import App from './App.vue';
import router from './router';
import store from './store';

import './assets/styles/style.scss';

Vue.config.productionTip = false;

Vue.use(VueI18n);

const messages = {
  kz: {
    lang: Kz,
  },
  ru: {
    lang: Ru,
  },
};

const i18n = new VueI18n({
  locale: 'ru',
  messages: messages,
});

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
