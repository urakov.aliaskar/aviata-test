import { shallowMount, createLocalVue } from '@vue/test-utils';
import Home from '@/views/Home.vue';

describe('Home.vue', () => {
  let mockStore, localVue, wrapper, getAirlines, getFlights;
  beforeEach(() => {
    getAirlines = jest.spyOn(Home.methods, 'getAirlines');
    getFlights = jest.spyOn(Home.methods, 'getFlights');
    mockStore = { dispatch: jest.fn() };
    localVue = createLocalVue();
    wrapper = shallowMount(Home, {
      localVue,
      mocks: {
        $store: mockStore,
      },
    });
  });

  it('Check tickets', () => {
    expect(wrapper.find('.tickets').exists()).toBe(true);
  });
  it('Check container', () => {
    expect(wrapper.find('.container').exists()).toBe(true);
  });
  it('Check filter', () => {
    expect(wrapper.find('.filter').exists()).toBe(true);
  });
  it('calls getAirlines on mount', () => {
    expect(getAirlines).toHaveBeenCalled();
  });
  it('calls getFlights on mount', () => {
    expect(getFlights).toHaveBeenCalled();
  });
});
